package lcd.activities;

import edu.wpi.first.wpilibj.interfaces.Gyro;
import lcd.Align;
import lcd.LCDContext;
import lcd.ScreenActivity;

/**
 * Display your FRC gyro value
 */
public class GyroCheckActivity extends ScreenActivity {
    
    private Gyro gyro; // gyro to be displayed

     /**
     * @see ScreenActivity
     */
    public GyroCheckActivity(LCDContext context, Gyro gyro) {
        super(context);
        this.gyro = gyro;

        setFirstlineAlign(Align.LEFT);
    }

      /**
     * @see ScreenActivity
     */
    public GyroCheckActivity(LCDContext context, ScreenActivity activity, Gyro gyro) {
        super(context, activity);
        this.gyro = gyro;

        setSecondLineAlign(Align.LEFT);
    }

    @Override
    public String getLine1() {
        return "Angle: " + gyro.getAngle();
    }

    @Override
    public String getLine2() {
        return "Rate: " + gyro.getRate();
    }

}