package lcd.activities;

import edu.wpi.first.wpilibj.PowerDistributionPanel;
import lcd.LCDContext;
import lcd.ScreenActivity;

/**
 * Display current PDP Voltage
 */
public class VoltageCheckActivity extends ScreenActivity {

    private PowerDistributionPanel powerDistributionPanel;

    /**
     * @see ScreenActivity
     */
    public VoltageCheckActivity(LCDContext context) {
        super(context);
        powerDistributionPanel = new PowerDistributionPanel();
    }

     /**
     * @see ScreenActivity
     */
    public VoltageCheckActivity(LCDContext context, ScreenActivity activity) {
        super(context, activity);
        powerDistributionPanel = new PowerDistributionPanel();
    }

    @Override
    public String getLine1() {
        if(powerDistributionPanel.getVoltage() <= 10){
            return "Critical Voltage";
        }
        return "Current Voltage:";
    }

    @Override
    public String getLine2() {
        return round2Digits(powerDistributionPanel.getVoltage()) + " V";
    }

    // rounding
    private double round2Digits(double d){
        int tmp = (int)d * 100;

        return tmp/100.0;
    }

}