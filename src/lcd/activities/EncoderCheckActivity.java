package lcd.activities;

import edu.wpi.first.wpilibj.Encoder;
import lcd.LCDContext;
import lcd.ScreenActivity;

/**
 * Display your Encoder value
 */
public class EncoderCheckActivity extends ScreenActivity{

    private String encoderName; // Name of the Encoder
    private Encoder encoder; // Encoder

     /**
     * @see ScreenActivity
     */
    public EncoderCheckActivity(LCDContext context, String encoderName, Encoder encoder) {
        super(context);

        this.encoderName = encoderName;
        
        this.encoder = encoder;
 
    }

     /**
     * @see ScreenActivity
     */
    public EncoderCheckActivity(LCDContext context, ScreenActivity activity, String encoderName, Encoder encoder) {
        super(context, activity);

        this.encoderName = encoderName;

        this.encoder = encoder;

    }

    @Override
    public String getLine1() {
        return encoderName + " Enc:";
    }

    @Override
    public String getLine2() {
        return encoder.getDistance() + "";
    }


}