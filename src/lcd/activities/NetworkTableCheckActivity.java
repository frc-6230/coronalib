package lcd.activities;

import edu.wpi.first.networktables.NetworkTableInstance;
import lcd.LCDContext;
import lcd.ScreenActivity;

public class NetworkTableCheckActivity extends ScreenActivity {

    private NetworkTableInstance instance; // instance of NetworkTables
    private String tableName; // current TableName

     /**
     * @see ScreenActivity
     */
    public NetworkTableCheckActivity(LCDContext context, String tableName) {
        super(context);
        instance = NetworkTableInstance.getDefault();
        this.tableName = tableName;
    }

     /**
     * @see ScreenActivity
     */
    public NetworkTableCheckActivity(LCDContext context, ScreenActivity activity, String tableName) {
        super(context, activity);
        instance = NetworkTableInstance.getDefault();
        this.tableName = tableName;
    }

    @Override
    public String getLine1() {
        return tableName + ":";
    }

    @Override
    public String getLine2() {
        return "Connection: " + (instance.getTable(tableName).getKeys().size() > 0?"On":"Off");
    }

}