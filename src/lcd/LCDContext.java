
package lcd;

import java.util.LinkedList;

import edu.wpi.first.wpilibj.Timer;

/**
 * Interface to control connected LCD to roborio. 
 */
public class LCDContext{
    
    private ScreenActivity currentActivity; // current Activity running
    private LinkedList<LCDButton> buttons = new LinkedList<>(); // buttons that connected to the LCD;
    private LCDDisplay display; // hardware display

    private double updateRate; // how often the display will update default = 0.5 seconds
    private boolean refreshed;
    private double fpgaTimestamp; // time

    /**
     * @param activity Main Activity of the LCD
     * @param display LCDDisplay hardware class
     */
    public LCDContext(ScreenActivity activity, LCDDisplay display){
        this.currentActivity = activity;
        this.display = display;
        this.updateRate = 0.5;
    }

    /** 
     * @param activity Main Activity of the LCD
     * @param display LCDDisplay hardware class
     * @param updateRate the time between each lcd render
     */
    public LCDContext(ScreenActivity activity, LCDDisplay display, double updateRate){
        this.currentActivity = activity;
        this.updateRate = updateRate;
        this.display = display;
    }

    /**
     * runs periodically, updates the lcd view and behavior
     * @param fpgaTimestamp the result from Timer.getFPGATimeStamp()
     */
    public void update(){
        if (currentActivity == null) {
            return;
        }

        // updating the currentActivity
        currentActivity.updateActivity();

        // updating the buttons
        for(LCDButton button : buttons){
            if(button.getParent() == currentActivity){
                button.update();
            }
        }

        if(!currentActivity.isRefreshable() && !refreshed){
            refreshed = true;
            currentActivity.render();
        }else if (refreshed){
            return;
        }
        

        double currentTime = Timer.getFPGATimestamp();

        if( currentTime - this.fpgaTimestamp < updateRate) return;

        this.fpgaTimestamp = currentTime;

        currentActivity.render();
        
        
    }

    public void updateText(String firstLine, String secondLine){

        display.clear();

        display.LCDwriteString(firstLine, 1);
        display.LCDwriteString(secondLine, 2);
    }
    

    /**
     * @param currentActivity the currentActivity to set
     */
    public void setCurrentActivity(ScreenActivity currentActivity) {
        this.currentActivity = currentActivity;
        refreshed = false;
    }
    
    /**
     * @return the currentActivity
     */
    public ScreenActivity getCurrentActivity() {
        return currentActivity;
    }

    /** 
     * @param button your LCDButton
     */
    public void addButton(LCDButton button){
        buttons.add(button);
    }

    /**
     * @param button your LCDButton
     */
    public void removeButton(LCDButton button){
        buttons.remove(button);
    }

    /**
     * clears all the assigned buttons
     */
    public void removeAllButtons(){
        buttons.clear();
    }

    /**
     * clears all the buttons of the related activity
     * @param screenActivity the activity that the buttons needs to be deleted
     */
	public void clearButtonsFromActivity(ScreenActivity screenActivity) {
        LinkedList<LCDButton> toClear = new LinkedList<>();

        for(LCDButton button : buttons){
            if(button.getParent() == screenActivity){
                toClear.add(button);
            }
        }

        buttons.removeAll(toClear);

	}
}