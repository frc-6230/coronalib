package lcd;

/**
 * Keys that indicates the position of the text in LCD Display
 */
public enum Align{
    RIGHT, CENTER, LEFT;

    /**
     * formats String to fit into the align method
     * @param text your text String
     * @param align Align method
     * @return aligned String
     */
    public static String alignText(String text, Align align){
        
        switch(align){
            case RIGHT:
                return alignRight(text);
            case CENTER:
                return alignCenter(text);
            case LEFT:
                return alignLeft(text);
        }

        return text;
    }

    private static String alignRight(String text){
        int spaceSize = 16 - text.length();
        String space = "";

        if( spaceSize > 0){
            for(int i = 0; i < spaceSize; i++){
                space += " ";
            }
        }

        return space + text;
    }

    private static String alignLeft(String text){
        return text;
    }

    private static String alignCenter(String text){
        int spaceSize = (16 - text.length())/2;
        String space = "";

        if( spaceSize > 0){
            for(int i = 0; i < spaceSize; i++){
                space += " ";
            }
        }

        return space + text;
    }
}