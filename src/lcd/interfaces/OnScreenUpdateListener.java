package lcd.interfaces;

/**
 * Used to handle LCD Screen update
 */
public interface OnScreenUpdateListener{

    /**
     * 
     * @return the String the you want to display on the first line
     */
    String getLine1();

     /**
     * 
     * @return the String the you want to display on the second line
     */
    String getLine2();
}