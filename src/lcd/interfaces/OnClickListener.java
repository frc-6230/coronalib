package lcd.interfaces;

/**
 * This Listener is serving the LCDButton class, this interface defines the LCDButton behavior
 */
public interface OnClickListener{

    /**
     * Called when there is a click event
     */
    void onClick();

    /**
     * called pereodicly, updates the status of the digital input
     * @return the current digital input (true or false)
     */
    boolean getExternalInput();

}