package lcd;

import lcd.interfaces.OnClickListener;

/**
 * Defines DigitalInput, handles the communication between user input and lcd behavior
 */
public class LCDButton{
    
    private OnClickListener onClickListener; // listener that handles the button click

    private boolean previousInput; // the previous input from the digital input

    private ScreenActivity parent; // the activity that belongs to that button

    public LCDButton(ScreenActivity parent){
        this.parent = parent;
    }

    /**
     * runs periodicly, updates the input and handles button calls 
     */
    public void update(){
        
        boolean currentInput = onClickListener.getExternalInput(); // getting the current input
        
        // if the button was released (by knowing that it was holden before and now not)
        if(previousInput && !currentInput){

            // call onClick() method
            onClickListener.onClick();
            parent.render();
        }

        // update the input
        previousInput = currentInput;

    }

    /**
     * @param onClickListener the onClickListener to set
     */
    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    /**
     * @return the onClickListener
     */
    public OnClickListener getOnClickListener() {
        return onClickListener;
    }

    /**
     * @return the parent
     */
    public ScreenActivity getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(ScreenActivity parent) {
        this.parent = parent;
    }


}