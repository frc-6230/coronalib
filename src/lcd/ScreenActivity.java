package lcd;

import lcd.interfaces.OnScreenUpdateListener;

/**
 * Defines the view of the current Screen
 */
public abstract class ScreenActivity implements OnScreenUpdateListener{

    private ScreenActivity previousActivity; // the previous activity that was on
    private final LCDContext context; // context
    private Align firstLineAlign = Align.CENTER, secondLineAlign = Align.CENTER; // align arguments for rendering, default - CENTER

    private boolean refreshable = true; // if the screen should refresh or not
    /**
     * 
     * @param context your LCDContext object
     * @param previousActivity the activity that is calling from
     */
    public ScreenActivity(final LCDContext context, final ScreenActivity previousActivity){
        this.previousActivity = previousActivity;
        this.context = context;
    }

    /**
     * Creates new Activity without any previous Activities
     * @param context your LCDContext object
     */
    public ScreenActivity(final LCDContext context){
        this(context, null);
    }

    public final void render(){
        // formatting the strings to fit the align
        final String alignedLine1 = Align.alignText(getLine1(), firstLineAlign);
        final String alignedLine2 = Align.alignText(getLine2(), secondLineAlign);

        // updating the screen
        context.updateText(alignedLine1, alignedLine2);
    }


    /**
     * runs periodicly, updates the current activity before updating the context
     */
    public void updateActivity(){

    }

    /**
     * Finished the current activity and goes to the previous one.
     */
    public final void finish(){

        if(previousActivity != null){
            context.setCurrentActivity(previousActivity);
            context.clearButtonsFromActivity(this);
        }

    }

    /**
     * Starting new Activity, extends the previous activity to a new Activity, when the new activity
     * finished, the origin will replace it.
     * @param activity ScreenActivity Object
     */
    public final void extendActivity(final ScreenActivity activity){

        if(activity != null){

            // the previous activity will be always the object that called the method
            activity.previousActivity = this;

            context.setCurrentActivity(activity);
        }

    }

    /**
     * @return the firstlineAlign
     */
    public Align getFirstlineAlign() {
        return firstLineAlign;
    }

    /**
     * @return the secondLineAlign
     */
    public Align getSecondLineAlign() {
        return secondLineAlign;
    }

    /**
     * @param firstlineAlign the firstlineAlign to set
     */
    public void setFirstlineAlign(final Align firstlineAlign) {
        this.firstLineAlign = firstlineAlign;
    }

    /**
     * @param secondLineAlign the secondLineAlign to set
     */
    public void setSecondLineAlign(final Align secondLineAlign) {
        this.secondLineAlign = secondLineAlign;
    }

    /**
     * 
     * @param first first row Align
     * @param second second row Align
     */
    public void setAlign(final Align first, final Align second){
        setFirstlineAlign(first);
        setSecondLineAlign(second);
    }

    /**
     * @param refreshable the refreshable to set
     */
    public void setRefreshable(boolean refreshable) {
        this.refreshable = refreshable;
    }

    /**
     * @return the refreshable
     */
    public boolean isRefreshable() {
        return refreshable;
    }

}