/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package acceleration;

import edu.wpi.first.wpilibj.Timer;

/**
 * limits your acceleration in voltages
 */
public class AccelerationLimiter {

    private double maxAcceleration; // max acceleration that the motor should reach
    private double lastVelocity; // the previous velocity of the motor
    private double lastTime; // the last time checking the velocity

    /**
     * 
     * @param maxAcceleration max Acceleration
     */
    public AccelerationLimiter(double maxAcceleration){
        this.maxAcceleration = maxAcceleration;
        lastTime = Timer.getFPGATimestamp();
    }

    /**
     * 
     * @param velocity desierd velocity
     * @return the velocity that the motor is capable of right now
     */
    public double getFixedVelocity(double velocity){
        
        // getting the current acceleration, formula: (delta Velocity)/(delta Time)
        double currentAcceleration = (velocity - lastVelocity)/(Timer.getFPGATimestamp() - lastTime);

        lastTime = Timer.getFPGATimestamp(); // updating the last time

        // if the acceleration is above the max
        if (Math.abs(currentAcceleration) > maxAcceleration) {
            
            // add the max acceleration with the right direction - Math.sigum(value) -> returns -1 if the value is negative and 1 if it is positive
            lastVelocity += maxAcceleration * Math.signum(currentAcceleration);

            // return the fixed velocity
            return lastVelocity;
        }

        // else just update the last Velocity.
        lastVelocity = velocity;

        // no fix needed, return the velocity.
        return velocity;

    }

    /**
     * @param maxAcceleration the maxAcceleration to set
     */
    public void setMaxAcceleration(double maxAcceleration) {
        this.maxAcceleration = maxAcceleration;
    }

    /**
     * @return the maxAcceleration
     */
    public double getMaxAcceleration() {
        return maxAcceleration;
    }

}
