/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/
package drive;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.controller.RamseteController;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.kinematics.MecanumDriveOdometry;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

/**
 * Controlled Drivatrain using encoders and gyro, has usfull methods like pathfinding and driving by velocity
 */
public class ControlledDriveTrain extends SubsystemBase implements Driveable{
  private Encoder encRight;
  private Encoder encLeft;
  private PIDController leftController, rightController;
  private Gyro gyro;
  private SpeedController victorRight;
  private SpeedController victorLeft;
  private DifferentialDriveKinematics kinematics;
  private ChassisSpeeds chassisSpeeds;
  private SimpleMotorFeedforward feedforward;
  private DifferentialDriveOdometry m_odometry;


  /**
   * Creates a new driveTrain.
   */
  public ControlledDriveTrain(Encoder encRight, Encoder encLeft,  PIDController leftController, PIDController rightController, Gyro gyro, SpeedController victorRight, SpeedController victorLeft, DifferentialDriveKinematics kinematics, ChassisSpeeds chassisSpeeds, SimpleMotorFeedforward feedforward, MecanumDriveOdometry m_odometry) {
     
    this.encLeft = encLeft;
    this.encRight = encRight;
    this.leftController = leftController;
    this.rightController = rightController;
    this.gyro = gyro;
    this.victorRight = victorRight;
    this.victorLeft = victorLeft;
    this.kinematics = kinematics;
    this.chassisSpeeds = chassisSpeeds;
    
  }

  @Override
  public void driveVoltageLeft(double voltage) {
    victorLeft.setVoltage(voltage);
  }

  @Override
  public void driveVoltageRight(double voltage) {
    victorRight.setVoltage(voltage);
  }

  @Override
  public Encoder getLeftEncoder() {
    return encLeft;
  }

  @Override
  public Encoder getRightEncoder() {
    return encRight;
  }

  @Override
  public Gyro getGyro() {
    return gyro;
  }



  /**
   * This method allows for controlled velocity of left motor
   * @param velocity the desired velocity, double
   */

  public void controlSpeedLeft(double velocity)
  {
    driveVoltageLeft(feedforward.calculate(velocity) + leftController.calculate(encLeft.getRate(), velocity));
  }

  /**
   * This method allows for controlled velocity of right motor
   * @param velocity the desired velocity, double
   */
  
  public void controlSpeedRight(double velocity)
  {
    
    driveVoltageRight(feedforward.calculate(velocity) + rightController.calculate(encRight.getRate(), velocity));
    
  }

  /**
   * This method allows for control of the voltage percentage outputed by the left motor
   * @param velocity the desired velocity, double
   */

  public void drivePercentageLeft(double percentage)
  {
    victorLeft.set(percentage);
  }

  /**
   * This method allows for control of the voltage percentage outputed by the right motor
   * @param velocity the desired velocity, double
   */

  public void drivePercentageRight(double percentage)
  {
    victorRight.set(percentage);
  }

  /**
   * This method allows for control of the voltage percentage outputed by all motors
   * @param velocity the desired velocity, double
   */

  public void drivePercentage(double left, double right)
  {
    victorRight.set(right);
    victorLeft.set(left);
  }

  /**
   * This method allows control of voltage given to all motors
   * @param voltage the desired voltage, v
   */

  public void driveVoltage(double left, double right)
  {
    victorRight.setVoltage(right);
    victorLeft.setVoltage(left);
  }

  /**
   * This method allows for controlled velocity of all motors
   * @param velocity the desired velocity, m/s
   */
  public void controlSpeedAll(double left, double right)
  {
    
    victorRight.setVoltage(feedforward.calculate(right) + rightController.calculate(encRight.getRate(), right));
    victorLeft.setVoltage(feedforward.calculate(left) + leftController.calculate(encLeft.getRate(), left));
  }

  /**
   * This method allows for differentialDrive, conversion from desired linear and angular velocities to necessary speed outputed by all motors
   * @param linearVelocity the desired linear velocity, m/s
   * @param angularVelocity the desired angular velocity, radians
   */

  public void differentialDrive(double linearVelocity, double angularVelocity)
  {
    var chassisSpeeds = new ChassisSpeeds(linearVelocity, 0, angularVelocity);
    DifferentialDriveWheelSpeeds wheelSpeeds = kinematics.toWheelSpeeds(chassisSpeeds);
    
    controlSpeedAll(wheelSpeeds.leftMetersPerSecond, wheelSpeeds.rightMetersPerSecond);
  } 

  public DifferentialDriveWheelSpeeds getWheelSpeeds() {
    return new DifferentialDriveWheelSpeeds(encLeft.getRate(), encRight.getRate());
  }
  
  public Pose2d getPose() {
    return m_odometry.getPoseMeters();
  }

  /**
   * @return the leftController
   */
  public PIDController getLeftController() {
    return leftController;
  }

  /**
   * @return the rightController
   */
  public PIDController getRightController() {
    return rightController;
  }


  /**
   * @param leftController the leftController to set
   */
  public void setLeftController(PIDController leftController) {
    this.leftController = leftController;
  }

  /**
   * @param rightController the rightController to set
   */
  public void setRightController(PIDController rightController) {
    this.rightController = rightController;
  }


  public void setEncRight(Encoder enc)
  {
    this.encRight = enc;
  }

  public void setEncLeft(Encoder enc)
  {
    this.encLeft = enc;
  }

  public void setGyro(Gyro gyro)
  {
    this.gyro = gyro;
  }

  public SpeedController getVictorRight()
  {
    return victorRight;
  }

  public SpeedController getVictorLeft()
  {
    return victorLeft;
  }

  public void setVictorRight(SpeedController victor)
  {
    this.victorRight = victor;
  }

  public void setVictorLeft(SpeedController victor)
  {
    this.victorLeft = victor;
  }

  public DifferentialDriveKinematics getKinematics()
  {
    return kinematics;
  }

  public void setKinematics(DifferentialDriveKinematics kinematics)
  {
    this.kinematics = kinematics;
  }

  public ChassisSpeeds getChassisSpeed()
  {
    return chassisSpeeds;
  }

  public void setChassisSpeed(ChassisSpeeds chassisSpeeds)
  {
    this.chassisSpeeds = chassisSpeeds;
  }

  public void updateOdometry() {
    m_odometry.update(getAngle(), encLeft.getDistance(), encRight.getDistance());
  }

    /**
   * Returns the angle of the robot as a Rotation2d.
   *
   * @return The angle of the robot.
   */
  public Rotation2d getAngle() {
    // Negating the angle because WPILib gyros are CW positive.
    return Rotation2d.fromDegrees(-gyro.getAngle());
  }

  public RamseteCommand getRamsteteCommand(Trajectory trajectory, double b, double zeta){
    RamseteCommand ramseteCommand = new RamseteCommand(
        trajectory,
        this::getPose,
        new RamseteController(b, zeta),
        feedforward,
        kinematics,
        this::getWheelSpeeds,
        leftController,
        rightController,
        // RamseteCommand passes volts to the callback
        this::driveVoltage,
        this
    );

    return ramseteCommand;
  }


  @Override
  public void periodic() {
    
    try{
      updateOdometry();
    }catch(Exception ignored){}

  }
}
