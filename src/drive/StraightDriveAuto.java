/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/
package drive;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class StraightDriveAuto extends CommandBase {

  private PIDController controller;
  private Driveable driveable;
  private double target;
  private double startTime;

  private double rightValue, leftValue;
  /**
   * Creates a new StraightDriveAuto.
   */
  public StraightDriveAuto(Driveable driveable, PIDController controller, double target) {
    
    this.controller = controller;
    this.target = target;

    this.driveable = driveable;

    try {
      addRequirements((SubsystemBase) driveable);
    } catch (Exception e) {
      
    }
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    controller.setSetpoint(target);

    rightValue = driveable.getRightEncoder().getDistance();
    leftValue = driveable.getLeftEncoder().getDistance();

    startTime = Timer.getFPGATimestamp();


  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    double avgDelta = ((driveable.getRightEncoder().getDistance() - rightValue) + (driveable.getLeftEncoder().getDistance() - leftValue))/2;
    double voltage = controller.calculate(avgDelta);

    driveable.driveVoltageLeft(voltage);
    driveable.driveVoltageRight(voltage);

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    driveable.driveVoltageLeft(0);
    driveable.driveVoltageRight(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {

    if(controller.atSetpoint()){
      return Timer.getFPGATimestamp() - startTime >= 0.5;
    }

    startTime = Timer.getFPGATimestamp();
    return false;
  }
}
