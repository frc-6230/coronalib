package drive;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.interfaces.Gyro;

public interface Driveable{


     /**
     * This method allows for control of voltage given to the left motor
     * @param voltage the desired voltage, double
     */
    public void driveVoltageLeft(double voltage);


     /**
     * This method allows for control of voltage given to the right motor
     * @param voltage the desired voltage, double
     */
    public void driveVoltageRight(double voltage);


    
    public Encoder getLeftEncoder();

    public Encoder getRightEncoder();

    public Gyro getGyro();
}