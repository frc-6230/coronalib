/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package LimelightUtil;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
public class LimelightUtil{
    
    static NetworkTable table = NetworkTableInstance.getDefault().getTable("limelight");
    
    public static double getTargetXPixels() {
    return table.getEntry("thor").getDouble(0);
    }

    public static double getTrackedTarget() {
        return (table.getEntry("tv")).getDouble(0);
    }

    public static void setLedState(int ledState){// led state can be 1 or 0 ,0 = on,1 = off 2= blink 3= force blink 
        table.getEntry("ledMode").setNumber(ledState);
    }
    // public static void turnOnLED() {
        // table.getEntry("ledMode").setNumber(0);
    // }

    // public static void turnOffLED() {
        // table.getEntry("ledMode").setNumber(1);
    // }
    public static double getDistanceToTarget(double heightOfCamera ,double heightOfTarget,double angleOfCamera){
        double angleofTarget =  getVerticalRotationToTarget();
        return (heightOfTarget - heightOfCamera) / Math.tan(Math.toRadians(angleOfCamera + angleofTarget));// i used the math in the limelight site
    }
    
    public static int getPipeline() {
        return table.getEntry("pipeline").getNumber(0).intValue();
    }
    public static double getVerticalRotationToTarget()
     {
        return table.getEntry("ty").getDouble(0);
    }
    public static double getTargetAreaPercentage() {
        return table.getEntry("ta").getDouble(0);
    }
    public static void setCamMode(int CamMode) {
        table.getEntry("camMode").setNumber(CamMode);
        // can be 1 or 0 . 1 = disable Vision proccesing  0 = enable Vision proccesing   and disable   Driver Cam
    }
    public static double getCamMode() {
        return table.getEntry("camMode").getDouble(0);
        
    }
    // public static void DriverCamMode() {
    //     table.getEntry("camMode").setNumber(1);// disable Vision proccesing    
    // }
    // public static void VisionProcessorMode() {
    //     table.getEntry("camMode").setNumber(0);// enable Vision proccesing   and disable   Driver Cam
    // }
    public static double getSkewRotation() {
        return table.getEntry("ts").getDouble(0);
    }
    public static double getPipelineMS() {
        return table.getEntry("tl").getDouble(0);
    }
    public static boolean haslimelightValidTargets() {
        double tv = table.getEntry("tv").getDouble(0);
        if (tv == 0.0){
            return false;
        }else {
            return true;
        }
    }
    
    public double getdegRotationToTarget() {
       return table.getEntry("tx").getDouble(0.0);
    }
    public static void SetPipeLine(int pipeline) {
        if(pipeline<0){
            pipeline = 0;
            throw new IllegalArgumentException("Pipeline can not be less than zero");//just tried here new thing i learned
        }else if(pipeline>9){
            pipeline = 9;
            throw new IllegalArgumentException("Pipeline can not be greater than nine");
        }
        table.getEntry("pipeline").setValue(pipeline);
    } 
    





}
