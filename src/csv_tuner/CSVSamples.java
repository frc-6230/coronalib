package csv_tuner;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

/**
 * writes csv samples into csv file
 */
public class CSVSamples {

    private String xAxisName, yAxisName; // names
    private LinkedList<Double> xAxis, yAxis; // actual values

    /**
     * Constructor
     * @param xAxisName your x axis graph name
     * @param yAxisName your y axis graph name
     */
    public CSVSamples(String xAxisName, String yAxisName) {
        this.xAxisName = xAxisName;
        this.yAxisName = yAxisName;

        xAxis = new LinkedList<>();

        yAxis = new LinkedList<>();
    }


    /**
     * takes sample and saves to the csv for later export
     * @param x your input
     * @param y your measured output
     */
    public void takeSample(double x, double y){
        xAxis.add(x);
        yAxis.add(y);
    }

    /**
     * remove sample by index
     * @param index sample index
     */
    public void removeSample(int index){
        if (index < xAxis.size()){
            xAxis.remove(index);
            yAxis.remove(index);
        }
    }

    /**
     * remove last sample
     */
    public void removeLastSample(){
        xAxis.removeLast();
        yAxis.removeLast();
    }

    /**
     * save your csv file in the robot
     * @param path where do you want to save the file
     * @return success
     */
    public boolean saveToCSV(String path){

        String output = xAxisName + "," + yAxisName + "\n";

        for (int i = 0; i < xAxis.size(); i++) {

            output += xAxis.get(i) + "," + yAxis.get(i) + "\n";

        }

        try {
            File myObj = new File(path);

            myObj.createNewFile();

            FileWriter myWriter = new FileWriter(path);
            myWriter.write(output);
            myWriter.close();
        } catch (IOException e) {
            return false;
        }

        return true;

    }


}